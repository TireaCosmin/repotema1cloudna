﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Grpc.Net.Client;

namespace PersonClient
{
    class Program
    {

        static bool verify(string cnp)
        {
            for (int index = 0; index < cnp.Length; ++index)
                if (char.IsDigit(cnp[index]) == false)
                {
                    return false;
                }
            return true;
        }

        static async Task Main(string[] args)
        {
            using var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new PersonRegistration.PersonRegistrationClient(channel);

            Console.Write("Enter your name please: ");
            var name = Console.ReadLine();

            Console.Write("Enter your CNP please: ");
            string cnp1 = Console.ReadLine();

            while (cnp1.Length != 13 || Program.verify(cnp1)==false )
            {
                Console.Write("Enter a valid CNP please (the CNP have 13 characters and characters are numbers ): ");
                cnp1 = Console.ReadLine();
            }
                     
            var sex = "";
            if (cnp1[0] - '0' == 1 || cnp1[0] - '0' == 5)
                sex = "Man";
            else
                if (cnp1[0] - '0' == 2 || cnp1[0] - '0' == 6)
                sex = "Woman";

            string year, month, day;
            if (cnp1[0] - '0' == 1 || cnp1[0] - '0' == 2)
            {
                year = "19" + cnp1[1] + cnp1[2];
                month = cnp1[3].ToString() + cnp1[4].ToString();
                day = cnp1[5].ToString() + cnp1[6].ToString();
                //Console.WriteLine(year + " " + month + " " + day);
            }
            else
            {
                year = "20" + cnp1[1] + cnp1[2];
                month = cnp1[3].ToString() + cnp1[4].ToString();
                day = cnp1[5].ToString() + cnp1[6].ToString();
                //Console.WriteLine(year + " " + month + " " + day);
            }
            DateTime currentdate = DateTime.Now;
            DateTime borndate = new DateTime(Int32.Parse(year), Int32.Parse(month), Int32.Parse(day));
            double zile = (currentdate - borndate).TotalDays;
            int varsta = (int)(zile / 365);
            //Console.WriteLine(varsta);

            var reply = await client.AddPersonAsync(new AddPersonRequest { Name = name, Cnp = cnp1, Gender = sex, Age = varsta });
            Console.WriteLine(reply.Message);
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}