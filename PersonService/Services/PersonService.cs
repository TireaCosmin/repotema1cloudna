using Grpc.Core;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonService
{
    public class PersonService : PersonRegistration.PersonRegistrationBase
    {
        private readonly ILogger<PersonService> _logger;
        public PersonService(ILogger<PersonService> logger)
        {
            _logger = logger;
        }

        public override Task<AddPersonReply> AddPerson(AddPersonRequest request, ServerCallContext context)
        {
            Console.WriteLine();
            Console.WriteLine(" A new person was added! Name "+request.Name+" with CNP: "+request.Cnp+" having the genre "+request.Gender+" and is "+request.Age+" years old!");
            Console.WriteLine();
            return Task.FromResult(new AddPersonReply()
            {
            Message = "Hello! Your name is " + request.Name + " , you have CNP " + request.Cnp + " and you are " + request.Gender + " and your age is " + request.Age + " years old !",
            });
        }
    }
}
